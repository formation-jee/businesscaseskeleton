/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.jsfbusinesscase.model;

/**
 *
 * @author aureliendelorme
 */
public class Candidat {
    String nom;
    String prenom;
   
    public Candidat(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
        public String getPrenom(){
        return this.prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}       
