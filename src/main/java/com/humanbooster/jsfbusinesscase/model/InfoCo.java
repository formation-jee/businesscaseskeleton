/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.jsfbusinesscase.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author aureliendelorme
 */
public class InfoCo {

    private Date date;
    private ArrayList<Candidat> candidats;
    private String lieu;

    public InfoCo() {
        this.candidats = new ArrayList<Candidat>();
    }

    public InfoCo(String lieu, Date date) {
        this.candidats = new ArrayList<Candidat>();
        this.lieu = lieu;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Candidat> getCandidats() {
        return candidats;
    }

    public void addCandidat(Candidat candidat) {
        this.candidats.add(candidat);
    }

    public void removeCandidat(Candidat candidat){
        this.candidats.remove(candidat);
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }


}
