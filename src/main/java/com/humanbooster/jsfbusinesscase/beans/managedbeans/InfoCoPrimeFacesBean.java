package com.humanbooster.jsfbusinesscase.beans.managedbeans;

import com.humanbooster.jsfbusinesscase.model.InfoCo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Date;

@ManagedBean
@SessionScoped
public class InfoCoPrimeFacesBean {
    private InfoCo infoCoAdd = new InfoCo();
    private InfoCo infoCoEdit;
    
    private ArrayList<InfoCo> infoCos = new ArrayList<InfoCo>();
    public InfoCoPrimeFacesBean() throws ParseException{
        String dateClermontString = "21/09/2021";  
        String dateLyonString = "21/10/2021";  
        Date dateClermont=new SimpleDateFormat("dd/MM/yyyy").parse(dateClermontString);  
        Date dateLyon=new SimpleDateFormat("dd/MM/yyyy").parse(dateLyonString);  
        
        this.infoCos.add(
                new InfoCo("Clermont-Ferrand", dateClermont)
        );
        
        this.infoCos.add(
            new InfoCo("Lyon", dateLyon)
        );
    }
    
    public InfoCo getInfoCoAdd() {
        return infoCoAdd;
    }

    public void setInfoCoAdd(InfoCo infoCoAdd) {
        this.infoCoAdd = infoCoAdd;
    }


    public ArrayList<InfoCo> getInfoCos() {
        return infoCos;
    }

    
    public void add(){
        this.info();
        this.infoCos.add(this.infoCoAdd);
        this.infoCoAdd = new InfoCo();
    }
    
    

    public void info() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info","Infoco ajoutée."));
    }

    public void delete(InfoCo infoCo) {
        this.infoCos.remove(infoCo);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Infoco supprimée.","Infoco supprimée."));
    }

    public void editInfoCo(InfoCo infoCo){
        FacesMessage msg = new FacesMessage("Infoco en cours d'édition", infoCo.getLieu());
        FacesContext.getCurrentInstance().addMessage(null, msg);

        this.infoCoEdit = infoCo;
    }

    public InfoCo getInfoCoEdit() {
        return infoCoEdit;
    }

    public void setInfoCoEdit(InfoCo infoCoEdit) {
        this.infoCoEdit = infoCoEdit;
    }

    public void setInfoCos(ArrayList<InfoCo> infoCos) {
        this.infoCos = infoCos;
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
