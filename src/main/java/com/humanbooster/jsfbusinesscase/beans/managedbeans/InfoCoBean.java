/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.jsfbusinesscase.beans.managedbeans;

import com.humanbooster.jsfbusinesscase.model.InfoCo;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.Date;

@ManagedBean
@ApplicationScoped
public class InfoCoBean {
    // List qui contiendra toutes nos infocos
    private ArrayList<InfoCo> infoCos = new ArrayList<InfoCo>();
    private String lieu;
    // Date saisie dans notre formulaire de création
    private Date date;
    // Infoco qui sera utilisé dans notre formulaire d'édition
    private InfoCo infocoToEdit;
    
    // On rajoute ça pour retrouver l'infoco à éditer 
    // Plus tard vous retrouverez cette infoco grâce à son ID
    private int indexListToEdit;
    

    //Getteurs / Setteurs de nos attributs privés
    public String getLieu() {
        return lieu;
    }
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public ArrayList<InfoCo> getInfoCos() {
        return this.infoCos;
    }
    public void setInfoCos(ArrayList<InfoCo> infoCos) {
        this.infoCos = infoCos;
    }
    public InfoCo getInfocoToEdit() {
        return infocoToEdit;
    }
    public void setInfocoToEdit(InfoCo infocoToEdit) {
        this.infocoToEdit = infocoToEdit;
    }    
    
    //Constructeur de notre bean managé
    public InfoCoBean() {
    }

    // Fonction qui ajoute une nouvelle infoco
    public String add() {
        this.infoCos.add(new InfoCo(this.lieu, this.date));
        return "infoco/info-co.xhtml";
    }
   
    
    // Fonction qui supprime une infoco dans notre liste
    public void delete(InfoCo infoCo) {
        this.infoCos.remove(infoCo);
    }
    
    
    // Fonction qui prend en paramètre l'infoco à éditer
    // et affiche le formulaire d'édition
    public String edit(InfoCo infoCo){
        this.indexListToEdit = this.infoCos.indexOf(infoCo);
        this.infocoToEdit = infoCo;
        return "infoco/edit-infoco.xhtml";
    }
   
    // Fonction qui met à jour notre liste d'infoco après soumission
    // d'une mise à jour
    public String edit() {
        this.infoCos.get(this.indexListToEdit).setDate(this.infocoToEdit.getDate());
        this.infoCos.get(this.indexListToEdit).setLieu(this.infocoToEdit.getLieu());
        return "infoco/info-co.xhtml";
    }
   
}
