/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.jsfbusinesscase.beans.managedbeans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



@ManagedBean
@SessionScoped
public class LoginBean implements Serializable{
    
    private String email;
    private String password;
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String checkUserAndPassword() {
        String redirectPath = null;
        if(this.email.equals("aurelien") && this.password.equals("aurelien")) {
            redirectPath = "../infoco/info-co.xhtml";
        } else {
            redirectPath = "security/login.xhtml";
        }
        return redirectPath;
    }
    
}
