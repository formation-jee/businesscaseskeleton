/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.humanbooster.jsfbusinesscase.beans.managedbeans;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import com.humanbooster.jsfbusinesscase.model.Candidat;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author aureliendelorme
 */
@ManagedBean
@SessionScoped
public class CandidatsBean {
  
    
    List<Candidat> candidats = new ArrayList<Candidat>();
    private String nom;
    private String prenom;

    public List<Candidat> getCandidats() {
        return this.candidats;
    }
    public String getNom(){
        return this.nom;
    }
    public void setNom(String nom){
        this.nom = nom;
    }
    public String getPrenom() {
        return this.prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String delete(Candidat candidat){
        this.candidats.remove(candidat);
        return null;
    }
    public void add() {
        this.candidats.add(new Candidat(this.nom, this.prenom));

    }
}
